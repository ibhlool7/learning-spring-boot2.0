package com.greglturnquist.learningspringboot.learningspringboot.chapter3

import org.springframework.data.repository.query.ReactiveQueryByExampleExecutor
import org.springframework.data.repository.reactive.ReactiveCrudRepository

interface EmployeeRepository : ReactiveCrudRepository<Employee,String>,ReactiveQueryByExampleExecutor<Employee> {

}