package com.greglturnquist.learningspringboot.learningspringboot.chapter2

import org.apache.commons.logging.Log
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RestController
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono
import java.util.logging.Logger


@RestController
class Controller {

    private val logger = Logger.getLogger(this::javaClass.toString())
    @GetMapping("/images")
    fun getImages(): Flux<Image> {
        return Flux.just(Image("1", "Bread"), Image("2", "Cock"))
    }

    @PostMapping("/image")
    fun create(@RequestBody images: Flux<Image>): Mono<Void> {
        val then = images.map { item ->
            logger.info("we will ${item.name} soon! ")
        }.then()
        logger.info("next step")
        return then
    }

}