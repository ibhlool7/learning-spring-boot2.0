package com.greglturnquist.learningspringboot.learningspringboot.chapter2

data class Image(val id: String, var name: String)