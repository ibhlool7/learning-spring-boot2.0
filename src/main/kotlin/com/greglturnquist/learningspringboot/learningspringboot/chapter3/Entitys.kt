package com.greglturnquist.learningspringboot.learningspringboot.chapter3

import org.springframework.data.annotation.Id
import org.springframework.data.mongodb.core.mapping.Document

@Document(collection = "employees")
data class Employee(@Id val id: String? = null, var firstName: String, var lastName: String,var role:String)

@Document
data class Image3(@Id val id: String? = null, var name: String)