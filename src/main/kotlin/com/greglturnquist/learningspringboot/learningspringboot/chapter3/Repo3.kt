package com.greglturnquist.learningspringboot.learningspringboot.chapter3

import org.springframework.data.repository.reactive.ReactiveCrudRepository
import reactor.core.publisher.Mono

interface ImageRepo3 : ReactiveCrudRepository<Image3, String> {
    fun findByName(name: String): Mono<Image3>
}