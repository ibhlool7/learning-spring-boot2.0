package com.greglturnquist.learningspringboot.learningspringboot.noteOnReactor

import org.reactivestreams.Subscriber
import org.reactivestreams.Subscription
import reactor.core.publisher.Flux
import reactor.core.scheduler.Schedulers

fun main(vararg args: String) {
    Flux.just("dog", "cat", "caw").log().map {
        return@map it.toUpperCase()
    }.subscribeOn(Schedulers.newParallel("sub")).publishOn(Schedulers.newParallel("pub", 2)).toStream()

}

internal class SelfSubscriber : Subscriber<String> {
    var count = 0
    lateinit var rootSubscriber: Subscription
    override fun onComplete() {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onSubscribe(s: Subscription?) {
        this.rootSubscriber = s!!
        s.request(2)
    }

    override fun onNext(t: String?) {
        if (this.count < 2) {
        } else {
            this.count = 0
            this.rootSubscriber.request(2)
        }
    }

    override fun onError(t: Throwable?) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

}