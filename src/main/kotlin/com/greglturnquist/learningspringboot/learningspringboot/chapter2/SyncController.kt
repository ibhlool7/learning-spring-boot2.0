package com.greglturnquist.learningspringboot.learningspringboot.chapter2

import org.springframework.core.io.Resource
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.GetMapping
import java.time.Duration

@Controller
class SyncController(val imageService: ImageService) {
    @GetMapping("sync/all")
    fun fetchAllImage(): MutableList<Image>? {
        return imageService.findAllImage().collectList().block(Duration.ofSeconds(10))
    }

    @GetMapping("sync/one")
    fun getAImage(name: String): Resource? {
        return imageService.findOneImage(name).block()
    }
}