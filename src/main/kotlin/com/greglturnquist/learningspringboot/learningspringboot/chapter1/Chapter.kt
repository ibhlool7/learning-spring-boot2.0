package com.greglturnquist.learningspringboot.learningspringboot.chapter1

import org.springframework.data.annotation.Id
import org.springframework.data.mongodb.core.mapping.Document

@Document
data class Chapter(@Id val id: String? = null, var name: String)