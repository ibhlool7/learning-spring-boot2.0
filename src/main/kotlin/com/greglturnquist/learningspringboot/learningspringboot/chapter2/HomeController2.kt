package com.greglturnquist.learningspringboot.learningspringboot.chapter2

import org.springframework.core.io.InputStreamResource
import org.springframework.http.MediaType
import org.springframework.http.ResponseEntity
import org.springframework.http.codec.multipart.FilePart
import org.springframework.stereotype.Controller
import org.springframework.ui.Model
import org.springframework.web.bind.annotation.*
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono
import java.io.IOException
import java.io.InputStream

@Controller
class HomeController2(private val imageService: ImageService) {
    companion object {
        private const val BASE_PATH = "/images"
        private const val FILE_NAME = "{filename:.+}"
    }

    @GetMapping(value = ["$BASE_PATH/$FILE_NAME/raw"], produces = [MediaType.IMAGE_JPEG_VALUE])
    @ResponseBody
    fun oneRawImage(@PathVariable name: String): Mono<ResponseEntity<*>> {
        return imageService.findOneImage(name).map {
            try {
                ResponseEntity.ok().contentLength(it.contentLength()).body(InputStreamResource(it.inputStream))
            } catch (e: IOException) {
                ResponseEntity.badRequest().body("Couldn't find $name => ${e.message}")
            }
        }
    }

    @PostMapping(value = [BASE_PATH])
    fun createFile(@RequestPart(name = "file") files: Flux<FilePart>): Mono<String> {
        return imageService.createImage(files).then(Mono.just("redirect:/"))
    }

    @DeleteMapping("$BASE_PATH/$FILE_NAME")
    fun deleteFile(@PathVariable fileName: String): Mono<String> {
        return imageService.deleteImage(fileName).then(Mono.just("redirect:/"))
    }

    @GetMapping("/")
    fun index(model: Model): Mono<String> {
        println("hello")
        model.addAttribute("images", imageService.findAllImage())
        return Mono.just("index")
    }
}