package com.greglturnquist.learningspringboot.learningspringboot.chapter1

import org.springframework.data.repository.reactive.ReactiveCrudRepository

interface ChapterRepo :ReactiveCrudRepository<Chapter,String> {

}