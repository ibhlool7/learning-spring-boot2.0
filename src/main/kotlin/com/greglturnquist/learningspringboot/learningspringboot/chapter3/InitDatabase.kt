package com.greglturnquist.learningspringboot.learningspringboot.chapter3

import org.springframework.boot.CommandLineRunner
import org.springframework.context.annotation.Bean
import org.springframework.data.mongodb.core.MongoOperations
import org.springframework.stereotype.Component
import java.util.*


@Component
class InitDatabase {
    @Bean
    fun initialize3(operations: MongoOperations) = CommandLineRunner {
        operations.dropCollection(Image3::class.java)
        operations.insert(Image3("1",
                "learning-spring-boot-cover.jpg"));
        operations.insert(Image3("2",
                "learning-spring-boot-2nd-edition-cover.jpg"));
        operations.insert(Image3("3",
                "bazinga.png"));
        operations.findAll(Image3::class.java).forEach { println(it.toString()) }

        operations.dropCollection(Employee::class.java)
        val e1 = Employee(UUID.randomUUID().toString(), "Bilbo", "Baggins", "burglar")
        val e2 = Employee(UUID.randomUUID().toString(), "Frodo", "Baggins", "ring bearer")
        operations.insert(e1)
        operations.insert(e2)
    }
}