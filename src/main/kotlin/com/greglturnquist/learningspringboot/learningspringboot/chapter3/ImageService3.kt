package com.greglturnquist.learningspringboot.learningspringboot.chapter3

import io.micrometer.core.instrument.MeterRegistry
import org.springframework.core.io.ResourceLoader
import org.springframework.http.codec.multipart.FilePart
import org.springframework.stereotype.Service
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono
import java.io.File
import java.io.IOException
import java.nio.file.Files
import java.nio.file.Paths
import java.util.*

@Service
class ImageService3(val imageRepo3: ImageRepo3, val resourceLoader: ResourceLoader, private val meterRegistry: MeterRegistry) {
    companion object {
        private const val UPLOAD_ROOT = "/images"
        private const val FILE_NAME = "{filename:.+}"
    }

    fun fundAllImage() = imageRepo3.findAll()

    fun createImage(files: Flux<FilePart>): Mono<Void> {
        return files.flatMap {
            val savesImage = imageRepo3.save(Image3(UUID.randomUUID().toString(), it.name()))
            val copyFile = Mono.just(Paths.get(UPLOAD_ROOT, it.filename()).toFile()).log("createImage").map {
                try {
                    it.createNewFile()
                    it
                } catch (ex: IOException) {
                    throw RuntimeException(ex)
                }
            }.log("createImage-newfile").flatMap(it::transferTo).log("createImage-copy")

            val countFile: Mono<Void> = Mono.fromRunnable {
                meterRegistry.summary("file.uploaded.bytes").record(Paths.get(UPLOAD_ROOT).toFile().length().toDouble())
            }

            return@flatMap Mono.`when`(savesImage, copyFile)
        }.then()
    }

    fun deleteImage(fileName: String): Mono<Void> {
        val fromDb = imageRepo3.findByName(fileName).flatMap {
            imageRepo3.delete(it)
        }
        val deletedFile: Mono<Void> = Mono.fromRunnable {
            try {
                Files.deleteIfExists(Paths.get(UPLOAD_ROOT, fileName))
            } catch (ex: Exception) {
                throw RuntimeException(ex)
            }
        }
        return Mono.`when`(fromDb, deletedFile).then()
    }

}
