package com.greglturnquist.learningspringboot.learningspringboot

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.context.annotation.Bean
import org.springframework.web.filter.reactive.HiddenHttpMethodFilter


@SpringBootApplication
class LearningSpringBootApplication

fun main(args: Array<String>) {
    runApplication<LearningSpringBootApplication>(*args)
}

@Bean
fun hiddenHttpMethodFilter(): HiddenHttpMethodFilter {
    return HiddenHttpMethodFilter()
}