package com.greglturnquist.learningspringboot.learningspringboot.chapter2

import org.springframework.boot.CommandLineRunner
import org.springframework.context.annotation.Bean
import org.springframework.core.io.Resource
import org.springframework.core.io.ResourceLoader
import org.springframework.http.codec.multipart.FilePart
import org.springframework.stereotype.Service
import org.springframework.util.FileCopyUtils
import org.springframework.util.FileSystemUtils
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono
import java.io.File
import java.io.FileWriter
import java.io.IOException
import java.lang.Exception
import java.nio.file.Files
import java.nio.file.Path
import java.nio.file.Paths
import java.util.logging.Logger

@Service
class ImageService(val recourseLoader: ResourceLoader) {
    private val UPLOAD_ROOT = "upload-dir"
    private val logger = Logger.getLogger(this.javaClass.toString())

    @Bean
    fun setup(): CommandLineRunner {
        return CommandLineRunner {
            FileSystemUtils.deleteRecursively(File(UPLOAD_ROOT))
            Files.createDirectory(Paths.get(UPLOAD_ROOT))

            FileCopyUtils.copy("test file", FileWriter("$UPLOAD_ROOT/learning-spring-boot-cover.jpg"))
            FileCopyUtils.copy("test file2", FileWriter(UPLOAD_ROOT +
                    "/learning-spring-boot-2nd-edition-cover.jpg"))
            logger.info("copy file done !")
        }
    }

    fun findAllImage(): Flux<Image> {
        return try {
            Flux.fromIterable(Files.newDirectoryStream(Paths.get(UPLOAD_ROOT)).map {
                Image(it.hashCode().toString(), it.fileName.toString())
            })
        } catch (e: IOException) {
            Flux.empty<Image>()
        }
    }

    fun findOneImage(name: String): Mono<Resource> {
        return Mono.fromSupplier {
            recourseLoader.getResource("file:$UPLOAD_ROOT/$name")
        }
    }

    fun createImage(files: Flux<FilePart>): Mono<Void> {
        return files.flatMap {
            it.transferTo(Paths.get(UPLOAD_ROOT, it.filename()).toFile())
        }.then()
    }

    fun deleteImage(fileName: String): Mono<Void> {
        return Mono.fromRunnable {
            try {
                Files.deleteIfExists(Paths.get("$UPLOAD_ROOT/$fileName"))
            } catch (ex: Exception) {
                throw RuntimeException(ex)
            }
        }
    }
}