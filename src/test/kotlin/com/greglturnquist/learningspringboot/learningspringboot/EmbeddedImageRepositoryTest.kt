package com.greglturnquist.learningspringboot.learningspringboot

import com.greglturnquist.learningspringboot.learningspringboot.chapter3.Image3
import com.greglturnquist.learningspringboot.learningspringboot.chapter3.ImageRepo3
import org.junit.jupiter.api.BeforeAll
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.data.mongo.DataMongoTest
import org.springframework.data.mongodb.core.MongoOperations
import reactor.test.StepVerifier
import java.awt.Image
import java.util.ArrayList
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.BeforeEach
import org.springframework.test.context.event.annotation.BeforeTestClass
import org.springframework.test.context.event.annotation.BeforeTestMethod
import org.springframework.test.context.junit4.SpringRunner

import java.util.function.Supplier


@DataMongoTest
class EmbeddedImageRepositoryTest {

    @Autowired
    lateinit var repo: ImageRepo3

    @Autowired
    lateinit var operations: MongoOperations

    @BeforeTestClass
    fun setUp() {
        operations.dropCollection(Image3::class.java)
        operations.insert(Image3("1",
                "learning-spring-boot-cover.jpg"));
        operations.insert(Image3("2",
                "learning-spring-boot-2nd-edition-cover.jpg"));
        operations.insert(Image3("3",
                "bazinga.png"));
        operations.findAll(Image3::class.java).forEach {
            println(it.toString())
        }
    }

    @Test
    fun findAllShouldWork() {
        val image = repo.findAll()
        StepVerifier.create(image).recordWith { ArrayList() }.expectNextCount(3).consumeRecordedWith {
            assertThat(it).hasSize(3)
            assertThat(it).extracting(Image3::javaClass.name).contains("learning-spring-boot-cover.jpg",
                    "learning-spring-boot-2nd-edition-cover.jpg",
                    "bazinga.png")
        }.expectComplete().verify()
    }

    /**
     * Due to the functional nature of StepVerifier, we need to return a Boolean representing pass/fail in `return@expectNextMatches' part
     */
    @Test
    fun findByNameShouldWork() {
        val image = repo.findByName("bazinga.png")
        StepVerifier.create(image).expectNextMatches {
            assertThat(it.name == "bazinga.png")
            assertThat(it.name == "3")
            return@expectNextMatches true
        }
    }

}