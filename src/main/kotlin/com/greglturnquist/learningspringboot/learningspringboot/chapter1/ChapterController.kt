package com.greglturnquist.learningspringboot.learningspringboot.chapter1

import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RestController
import reactor.core.publisher.Flux

@RestController
class ChapterController(private val chapterRepo: ChapterRepo) {
    @GetMapping("/chapter")
    fun getChapter(): Flux<Chapter> {
        return chapterRepo.findAll()
    }
}