package com.greglturnquist.learningspringboot.learningspringboot

import com.greglturnquist.learningspringboot.learningspringboot.chapter3.Image3
import com.greglturnquist.learningspringboot.learningspringboot.chapter3.ImageRepo3
import org.assertj.core.api.Assertions
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.autoconfigure.mongo.embedded.EmbeddedMongoAutoConfiguration
import org.springframework.boot.test.autoconfigure.data.mongo.DataMongoTest
import org.springframework.data.mongodb.core.MongoOperations
import org.springframework.test.context.event.annotation.BeforeTestClass
import reactor.test.StepVerifier
import java.util.ArrayList

/**
 * @RunWith(SpringRunner.class) has removed in Junit5
 * excludeAutoConfiguration = [EmbeddedMongoAutoConfiguration::class] for prevent using embedded MongoDB and
 * using real MongoDB
 */
@DataMongoTest(excludeAutoConfiguration = [EmbeddedMongoAutoConfiguration::class])
class LiveImageRepositoryTests {
    @Autowired
    lateinit var repository: ImageRepo3
    @Autowired
    lateinit var operations: MongoOperations


    @BeforeTestClass
    fun setUp() {
        operations.dropCollection(Image3::class.java)
        operations.insert(Image3("1",
                "learning-spring-boot-cover.jpg"));
        operations.insert(Image3("2",
                "learning-spring-boot-2nd-edition-cover.jpg"));
        operations.insert(Image3("3",
                "bazinga.png"));
        operations.findAll(Image3::class.java).forEach {
            println(it.toString())
        }
    }

    @Test
    fun findAllShouldWork() {
        val image = repository.findAll()
        StepVerifier.create(image).recordWith { ArrayList() }.expectNextCount(3).consumeRecordedWith {
            Assertions.assertThat(it).hasSize(3)
            Assertions.assertThat(it).extracting(Image3::javaClass.name).contains("learning-spring-boot-cover.jpg",
                    "learning-spring-boot-2nd-edition-cover.jpg",
                    "bazinga.png")
        }.expectComplete().verify()
    }

    /**
     * Due to the functional nature of StepVerifier, we need to return a Boolean representing pass/fail in `return@expectNextMatches' part
     */
    @Test
    fun findByNameShouldWork() {
        val image = repository.findByName("bazinga.png")
        StepVerifier.create(image).expectNextMatches {
            Assertions.assertThat(it.name == "bazinga.png")
            Assertions.assertThat(it.name == "3")
            return@expectNextMatches true
        }
    }

}