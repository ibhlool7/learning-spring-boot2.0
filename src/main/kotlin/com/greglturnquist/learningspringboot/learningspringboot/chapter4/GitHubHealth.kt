package com.greglturnquist.learningspringboot.learningspringboot.chapter4

import org.springframework.boot.actuate.health.Health
import org.springframework.boot.actuate.health.HealthIndicator
import java.net.HttpURLConnection
import java.net.URL

class GitHubHealth : HealthIndicator {
    override fun health(): Health {
        return try {
            val url = URL("https://www.github.com")
            val con: HttpURLConnection = url.openConnection() as HttpURLConnection
            val code = con.responseCode
            if (code in 200..299) {
                Health.up().build()
            } else {
                Health.down().withDetail("code", code).build()
            }
        } catch (ex: Exception) {
            Health.down(ex).build()
        }
    }

}