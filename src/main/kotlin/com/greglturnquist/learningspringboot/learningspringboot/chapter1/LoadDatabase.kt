package com.greglturnquist.learningspringboot.learningspringboot.chapter1

import org.springframework.boot.CommandLineRunner
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import reactor.core.publisher.Flux

/**
 * @Configuration marks this class as a source of beans
 * @Bean indicates that the return value of initialize() is a Spring Bean
 *
 */
@Configuration
class LoadDatabase {
    @Bean
    fun initialize(repository: ChapterRepo) = CommandLineRunner {
        repository.deleteAll()
        val arr = arrayOf(Chapter(name = "Java for science"), Chapter(name = "java cock book"))
        Flux.just(*arr).flatMap { item -> repository.save(item) }.subscribe { println() }
    }
}
