package com.greglturnquist.learningspringboot.learningspringboot

import com.greglturnquist.learningspringboot.learningspringboot.chapter2.HomeController2
import com.greglturnquist.learningspringboot.learningspringboot.chapter2.Image
import com.greglturnquist.learningspringboot.learningspringboot.chapter2.ImageService
import com.greglturnquist.learningspringboot.learningspringboot.chapter3.Image3
import org.junit.jupiter.api.Test
import org.mockito.BDDMockito.*
import org.mockito.Mockito
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.autoconfigure.thymeleaf.ThymeleafAutoConfiguration
import org.springframework.boot.test.autoconfigure.web.reactive.WebFluxTest
import org.springframework.boot.test.mock.mockito.MockBean
import org.springframework.context.annotation.Import
import org.springframework.test.web.reactive.server.WebTestClient
import reactor.core.publisher.Flux
import org.assertj.core.api.Assertions
import org.mockito.ArgumentMatchers
import org.springframework.core.io.ByteArrayResource
import org.springframework.core.io.Resource
import org.springframework.http.HttpHeaders
import reactor.core.publisher.Mono
import java.io.IOException


/**
 * The default configuration enables all @Controller beans and @RestController beans as well as
 *      a mock web environment, but with the rest of the autoconfiguration disabled.
 *  @Import(...) specifies what additional bits we want configured outside of any Spring WebFlux
 *      controllers. In this case, the Thymeleaf autoconfiguration is needed.
 */
@WebFluxTest(controllers = [HomeController2::class])
@Import(ThymeleafAutoConfiguration::class)
class HomeControllerTests {

    @Autowired
    lateinit var client: WebTestClient

    @MockBean
    lateinit var imageService: ImageService

    /**
     * The first three lines mock up the ImageService bean to return a Flux of two images when findAllImages gets called.
     *
     */
    @Test
    fun baseRouteShouldListAllImages() {
        val alphaImage = Image("1", "alpha.png")
        val bravoImage = Image("1", "bravo.png")
        given(imageService.findAllImage()).willReturn(Flux.just(alphaImage, bravoImage))

        val result = client.get().uri("/").exchange().expectStatus()
                .isOk.expectBody(String::class.java).returnResult()
        Mockito.verify(imageService).findAllImage()
        Mockito.verifyNoMoreInteractions(imageService)
        Assertions.assertThat(result.responseBody).contains("<title>Learning Spring Boot: Spring-a-Gram</title>")
                .contains("<a href=\"/images/alpha.png/raw\">")
                .contains("<a href=\"/images/bravo.png/raw\">")
    }

    @Test
    fun fetchingImageShouldWork() {
        given(imageService.findOneImage(ArgumentMatchers.any())).willReturn(Mono.just(ByteArrayResource("data".toByteArray())))
        val res: Boolean = client.get().uri("/images/alpha.png/raw").exchange().expectStatus().isOk.expectBody(String::class.java).isEqualTo("data");
        Mockito.verify(imageService).findOneImage("alpha.png")
        Mockito.verifyNoMoreInteractions(imageService)
    }

    fun fetchingNullImageShouldFail() {
        val resource = mock(Resource::class.java)
        given(resource.inputStream).willThrow(IOException("bad file"))
        given(imageService.findOneImage(ArgumentMatchers.any())).willReturn(Mono.just(resource))
        val bool: Boolean = client.get().uri("/images/alpha.png/raw").exchange().expectStatus().isBadRequest.expectBody(String::class.java).isEqualTo("/images/alpha.png/raw")
        verify(imageService).findOneImage("alpha.png");
        verifyNoMoreInteractions(imageService);

    }

    @Test
    fun deleteImageShouldWork() {
        val image = Image("1", "alpha.png")
        given(imageService.findOneImage(ArgumentMatchers.any())).willReturn(Mono.empty())
        client.delete().uri("/images/alpha.png").exchange().expectStatus().isSeeOther.expectHeader().valueEquals(HttpHeaders.LOCATION, "/")
        verify(imageService).deleteImage("alpha.png");
        verifyNoMoreInteractions(imageService);
    }
}