package com.greglturnquist.learningspringboot.learningspringboot.chapter3

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.domain.Example
import org.springframework.data.domain.ExampleMatcher
import org.springframework.data.mongodb.core.ReactiveMongoOperations
import org.springframework.data.mongodb.core.query.Criteria
import org.springframework.data.mongodb.core.query.Query
import org.springframework.data.mongodb.core.query.where
import org.springframework.stereotype.Service
import kotlin.reflect.KProperty

@Service
class QueryByExcmpleEmployee(val repo: EmployeeRepository) {

    @Autowired
    lateinit var reactiveMongoOperator: ReactiveMongoOperations

    fun example() {
        val employee = Employee(firstName = "hassan", lastName = "hossieni", role = "none")
        val exampleMatcher = ExampleMatcher.matching().withIgnoreCase()
                .withMatcher("lastName", ExampleMatcher.GenericPropertyMatchers.startsWith())
                .withIncludeNullValues()
        val of = Example.of(employee, exampleMatcher)
        val res = repo.findOne(of)
        val res2 = repo.findAll(of)
        reactiveMongoOperator.findOne(Query.query(where(Employee::firstName).`is`("Frodo")), Employee::class.java)
    }


}

