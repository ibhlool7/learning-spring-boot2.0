# Read Me First
The following was discovered as part of building this project:

* The original package name 'com.greglturnquist.learningspringboot.learning-spring-boot' is invalid and this project uses 'com.greglturnquist.learningspringboot.learningspringboot' instead.

# Getting Started

### Reference Documentation
For further reference, please consider the following sections:

* [Official Gradle documentation](https://docs.gradle.org)
* [Spring Boot Gradle Plugin Reference Guide](https://docs.spring.io/spring-boot/docs/2.2.5.RELEASE/gradle-plugin/reference/html/)
* [Coroutines section of the Spring Framework Documentation](https://docs.spring.io/spring/docs/5.2.4.RELEASE/spring-framework-reference/languages.html#coroutines)
* [Spring Data Reactive MongoDB](https://docs.spring.io/spring-boot/docs/2.2.5.RELEASE/reference/htmlsingle/#boot-features-mongodb)
* [Thymeleaf](https://docs.spring.io/spring-boot/docs/2.2.5.RELEASE/reference/htmlsingle/#boot-features-spring-mvc-template-engines)

### Guides
The following guides illustrate how to use some features concretely:

* [Handling Form Submission](https://spring.io/guides/gs/handling-form-submission/)

### Additional Links
These additional references should also help you:

* [Gradle Build Scans – insights for your project's build](https://scans.gradle.com#gradle)

# DEV Road
##Basics
- Constructor injection is used to automatically load it with a copy of `someClass` With Spring, if
  there is only one constructor call, there is no need to include an @Autowired annotation.
- @Service: This indicates this is a Spring bean used as a service. Spring Boot will automatically scan
  this class and create an instance.
- ResourceLoader: This is a Spring utility class used to manage files. It is created automatically by
  Spring Boot and injected to our service via constructor injection. This ensures our service starts
  off with a consistent state.
- Since this method only handles one image, it returns a Mono<Resource>. Remember, Mono is a container
  of one. Resource is Spring's abstract type for files.
- "{filename:.+}" (FILENAME): This is a pattern for filenames where the "." is included. Otherwise, Spring WebFlux will
  use the suffix as part of content negotiation (for example, .json would try to fetch a JSON response,
  while .xml would try to fetch an XML response).
- `const val` modifire must be use in which variable we want to use in annotations
- @Component ensures that this class will be picked up automatically by Spring Boot, and scanned for
  bean definitions
- For test cases, field injection is fine. But for actual running components, the Spring team
  recommends constructor injection
- `logging.level.<somePackage>`tells Spring Boot to adjust log levels with the name of the package tacked on followed
  by a level
##Monitoring
- We start by adding this dependency to our build.gradle as follows:
compile('org.springframework.boot:spring-boot-starter-actuator')
When you run this version of our app, the same business functionality is available that we saw earlier,
but there are additional HTTP endpoints.Endpoints, by default, are disabled. We have to opt in. This is accomplished by setting endpoints.
                                        {endpoint}.enabled=true inside `src/main/resources/application.properties`, like this:
                                        `endpoints.health.enabled=true`
                                        
##Reactive 
- To delay fetching the file until the client subscribes, we wrap it with Mono.fromSupplier(), and put
  getResource() inside a lambda
  
- `then()` lets us wait for the entire Flux to finish, yielding a Mono<Void>

##Test
- In general, it's recommended to use constructor injection for production code. But for test
  code where constructors are limited due to JUnit, autowiring as we've just done is fine.
- We use Reactor Test's StepVerifier to subscribe to the Flux from the repository and then assert against
 it.
 
