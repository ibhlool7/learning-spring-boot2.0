package com.greglturnquist.learningspringboot.learningspringboot

import com.greglturnquist.learningspringboot.learningspringboot.chapter3.Image3
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.ContextConfiguration
import java.util.*



@SpringBootTest
class ImageTest {
    @Test
    fun imagesManagedByLombokShouldWork(): Unit {
        val image = Image3("id", "name")
        assertThat(image.id.equals("id"))
        assertThat(image.name == "name")
    }

}