package com.greglturnquist.learningspringboot.learningspringboot.chapter1

import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController

@RestController // map into `/` path
class HomeController {
    @GetMapping
    fun greeting(@RequestParam(required = false, defaultValue = "") name: String): String {
        return when (name) {
            "" -> "hey!"
            else -> "hey $name!"
        }
    }
}